use serde::{Deserialize, Serialize};
use std::fmt::Debug;

#[cfg_attr(fuzzing, derive(fuzzcheck::DefaultMutator))]
#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(rename_all = "lowercase")]
pub enum RegisterKind {
    Guest,
    User,
}

#[cfg_attr(fuzzing, derive(fuzzcheck::DefaultMutator))]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub enum RegisterType {
    #[serde(rename = "m.login.dummy")]
    Dummy,
}

#[cfg_attr(fuzzing, derive(fuzzcheck::DefaultMutator))]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct AuthenticationData {
    pub session: String,
    #[serde(rename = "type")]
    pub register_type: RegisterType,
}

#[cfg_attr(fuzzing, derive(fuzzcheck::DefaultMutator))]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct DummyRegistrationJson {
    pub kind: RegisterKind,
    pub auth: AuthenticationData,
    pub device_id: String,
    pub inhibit_login: bool,
    pub initial_device_display_name: String,
    pub password: String,
    pub username: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct RegistrationResp {
    pub access_token: Option<String>,
    pub device_id: Option<String>,
    pub home_server: Option<String>,
    pub user_id: String,
}
