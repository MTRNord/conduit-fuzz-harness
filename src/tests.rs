use crate::harness;
use crate::types::RegistrationResp;
use axum::{
    body::Body,
    http::{self, Request, StatusCode},
};
use std::time::Duration;
use tokio::runtime::Builder;
use tower::ServiceExt;

#[test]
fn register() {
    let result = fuzzcheck::fuzz_test(register_fuzz)
        .default_options()
        .stop_after_duration(Duration::from_secs(60 * 60 * 24 * 7)) // 7 days
        .stop_after_first_test_failure(false)
        .launch();
    assert!(!result.found_test_failure);
}

fn register_fuzz(raw_data: &crate::types::DummyRegistrationJson) {
    let data = serde_json::to_string(&raw_data);
    if let Ok(data) = data {
        match Builder::new_multi_thread().enable_all().build() {
            Ok(rt) => {
                let _guard = rt.enter();
                rt.block_on(async move {
                    harness(
                        |client, s| async move {
                            let resp = client
                                .oneshot(
                                    Request::builder()
                                        .uri("/_matrix/client/r0/register")
                                        .header(
                                            http::header::CONTENT_TYPE,
                                            mime::APPLICATION_JSON.as_ref(),
                                        )
                                        .body(Body::from(s))
                                        .unwrap(),
                                )
                                .await
                                .unwrap();

                            if resp.status() == StatusCode::OK {
                                let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
                                let body: RegistrationResp = serde_json::from_slice(&body).unwrap();
                                println!("body: {:?}", body);
                                if !raw_data.inhibit_login {
                                    if body.access_token.is_none() {
                                        panic!("Missing access_token");
                                    }
                                    if body.device_id.is_none() {
                                        panic!("Missing device_id");
                                    }
                                    if body.user_id == String::new() {
                                        panic!("Missing user_id");
                                    }
                                } else if body.user_id == String::new() {
                                    panic!("Missing user_id");
                                }
                            }
                        },
                        "test_register",
                        data.as_bytes(),
                    )
                    .await;
                })
            }
            Err(error) => println!("Runtime error: {:?}", error),
        }
    }
}
