use crate::harness;
use crate::types::RegistrationResp;
use axum::{
    body::Body,
    http::{self, Request, StatusCode},
};
use tokio::runtime::Builder;
use tower::ServiceExt;

#[test]
fn reproduce_double_free() {
    let data = r#"{"kind":"user","auth":{"session":"","type":"m.login.dummy"},"device_id":"�\u001b���I%b","inhibit_login":false,"initial_device_display_name":"��\u0000���Ŧ.�Oo�JyE1","password":"��\u000f��","username":"��� }"}"#;
    println!("{:?}", data);
    match Builder::new_multi_thread().enable_all().build() {
        Ok(rt) => {
            let _guard = rt.enter();
            rt.block_on(async move {
                harness(
                    |client, s| async move {
                        let resp = client
                            .oneshot(
                                Request::builder()
                                    .uri("/_matrix/client/r0/register")
                                    .header(
                                        http::header::CONTENT_TYPE,
                                        mime::APPLICATION_JSON.as_ref(),
                                    )
                                    .body(Body::from(s))
                                    .unwrap(),
                            )
                            .await
                            .unwrap();

                        if resp.status() == StatusCode::OK {
                            let body = hyper::body::to_bytes(resp.into_body()).await.unwrap();
                            let body: RegistrationResp = serde_json::from_slice(&body).unwrap();
                            println!("body: {:?}", body);
                        }
                    },
                    "reproduce_double_free",
                    data.as_bytes(),
                )
                .await;
            })
        }
        Err(error) => println!("Runtime error: {:?}", error),
    }
}
