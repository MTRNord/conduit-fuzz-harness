#![cfg_attr(test, feature(no_coverage))]
#![cfg_attr(test, feature(trivial_bounds))]
#![cfg_attr(test, feature(type_alias_impl_trait))]

#[cfg(test)]
use axum::Router;
#[cfg(test)]
use std::{fs, future::Future};

#[cfg(test)]
use crate::conduit_magic::run;

#[cfg(test)]
pub mod conduit_magic;
#[cfg(test)]
pub mod types;

#[cfg(fuzzing)]
mod tests;

#[cfg(all(test, not(fuzzing)))]
mod verification_tests;

#[cfg(test)]
pub async fn harness<F, Fut>(fuzz_cb: F, path: &str, data: &[u8])
where
    F: Fn(Router, String) -> Fut,
    Fut: Future<Output = ()>,
{
    let tmpfs_path = format!("/tmp/db/{}", path);
    // Run Server
    let conduit = run(&tmpfs_path).await;

    if let Ok(s) = std::str::from_utf8(data) {
        let string: String = s.to_string();
        fuzz_cb(conduit, string).await;
    };

    if let Err(e) = fs::remove_dir(format!("{}/media", tmpfs_path)) {
        eprintln!("media: {:?}", e);
    }
    if let Err(e) = fs::remove_file(format!("{}/conduit.db", tmpfs_path)) {
        eprintln!("conduit.db: {:?}", e);
    }
    if let Err(e) = fs::remove_file(format!("{}/conduit.db-shm", tmpfs_path)) {
        eprintln!("conduit.db-shm: {:?}", e);
    }
    if let Err(e) = fs::remove_file(format!("{}/conduit.db-wal", tmpfs_path)) {
        eprintln!("conduit.db-wal: {:?}", e);
    }
}
