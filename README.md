# Conduit Fuzzing

This is a small harness to fuzz conduit

## What is used?

https://github.com/loiclec/fuzzcheck-rs

## Why is it not in conduit itself?

Currently this is blocked behind https://github.com/loiclec/fuzzcheck-rs/issues/6
as thats a soft requirement for adding this to ruma. So this would currently lead to
quite a lot of code duplication that doesnt belong upstream.

## How to run the tests:

Follow https://github.com/loiclec/fuzzcheck-rs and use
`cargo fuzzcheck tests::register` as a testcase.